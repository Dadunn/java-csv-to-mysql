/*
-- Query: show create table csvdatabase.x
-- Date: 2017-03-13 16:19
*/
INSERT INTO `X` (`Table`,`Create Table`) VALUES ('x','CREATE TABLE `x` (\n  `A` varchar(255) DEFAULT NULL,\n  `B` varchar(255) DEFAULT NULL,\n  `C` varchar(255) DEFAULT NULL,\n  `D` varchar(255) DEFAULT NULL,\n  `E` mediumtext,\n  `F` varchar(255) DEFAULT NULL,\n  `G` varchar(255) DEFAULT NULL,\n  `H` varchar(255) DEFAULT NULL,\n  `I` varchar(255) DEFAULT NULL,\n  `J` varchar(255) DEFAULT NULL\n) ENGINE=InnoDB DEFAULT CHARSET=utf8');
