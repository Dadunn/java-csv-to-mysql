import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.mysql.jdbc.Connection;
/*
Created by Destiny Dunn 
Updated 3/13/2017
*/
class main {
	/**
	 * Sends record to table by first formating so database can read in the values
	 *
	 *@param connection - connection to the database
	 *@param record	- record that is being sent
	 *@param stmt	- Statement that will execute the formated command with the data from the record
	 * 
	 */
	private static void sendToDatabase(Connection connection, CSVRecord record,
			Statement stmt) throws SQLException {
		String sqlIntoDatabase = "INSERT INTO X VAlUES (" + "'" + record.get(0)
				+ "'";
		for (int i = 1; i <= 9; i++) {
			sqlIntoDatabase += ", " + "'" + record.get(i).replace("'", "\\'")+ "'";
		}
		sqlIntoDatabase += ")";
		stmt.executeUpdate(sqlIntoDatabase);
	}
	
	/**
	 * Creates the table in the Database if the table doesn't exist
	 *
	 *@param connection - connection to the database
	 *@param stmt	- Statement that will execute the formated command with the data from the record
	 * 
	 */
	private static void createTable(Connection connection, Statement stmt)
			throws SQLException {
		DatabaseMetaData meta = connection.getMetaData();
		ResultSet res = meta.getTables(null, null, "X", null);
		if (!res.next()) {
			String sqlCreatTable = "CREATE TABLE X " + "(A VARCHAR(255), "
					+ " B VARCHAR(255), " + " C VARCHAR(255), "
					+ " D VARCHAR(255), " + " E MEDIUMTEXT, "
					+ " F VARCHAR(255), " + " G VARCHAR(255), "
					+ " H VARCHAR(255), " + " I VARCHAR(255), "
					+ " J VARCHAR(255)) ";
			stmt.executeUpdate(sqlCreatTable);
		}
	}
	
	/**
	 * Formats a record back into a CSV format
	 *
	 *@param record	- record that is being formatted
	 * 
	 */
	private static String CSVRecordToString(CSVRecord record) {
		String returnString = record.get(0);

		for (int i = 1; i < record.size(); i++) {
			returnString += "," + record.get(i);
		}
		returnString += "\n";
		return returnString;
	}
	
	/**
	 * Creates a file, and places a string specified by the caller into the file
	 *
	 *@param data - data being placed into the file
	 *@param filename - Name of the new file
	 * 
	 */
	private static void stringToFile(String data, String fileName) {
		File file = new File(fileName);
		try (PrintWriter out = new PrintWriter(fileName, "UTF-8");) {
			out.print(data);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Main function 
	 *
	 *@param args[0] - CSV file location
	 * 
	 */
	public static void main(String[] args) {
		
		String csvFileName = args[0];
		File file = new File(csvFileName);
		if (csvFileName == null || !file.exists() || !csvFileName.endsWith(".csv")) {
			System.out.println("Program requires an existing CSV File.");
			System.exit(0);
		}
		
		
		String url = "jdbc:mysql://localhost:3306/csvdatabase";

		final String USER = "java";
		final String PASS = "password";

		try (Connection connection = (Connection) DriverManager.getConnection(url, USER, PASS)) {
			Statement stmt = connection.createStatement();
			Reader in = new FileReader(csvFileName);
			CSVParser records = CSVFormat.RFC4180.withFirstRecordAsHeader().parse(in);
			String badData = "";
			
			//-1 for header
			int total = -1;
			int badDataTotal = -1;

			createTable(connection, stmt);

			for (CSVRecord record : records) {
				if (record.isConsistent()) {
					sendToDatabase(connection, record, stmt);	
				} else {
					badData += CSVRecordToString(record);
					badDataTotal++;
				}
				total++;
			}
			
			//Close Connection and FileReader
			in.close();
			connection.close();
			
			//Sending log to file, and sending bad data to file
			String timeStamp = new SimpleDateFormat("yyyy-MM-dd-HH-mm").format(new Date());
			stringToFile(badData, "bad-data(" + timeStamp + ").csv");
			String logString = total + " of records received\n" +
					(total-badDataTotal) + " of records successful\n" +
					badDataTotal + " of records failed";
			stringToFile(logString, "(" + timeStamp + ").log");
			
		} catch (Exception e) { // SQLException IOException needs to be seperated
			e.printStackTrace();
		}

	}

}
